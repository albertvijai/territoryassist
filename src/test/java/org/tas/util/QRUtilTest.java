/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.util;

import java.util.Properties;
import junit.framework.TestCase;

/**
 *
 * @author Albert Vijai
 */
public class QRUtilTest extends TestCase {
    
    public QRUtilTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of encodeQR method, of class QRUtil.
     */
    public void testEncodeQR() throws Exception {
        String url = "http//www.google.com";
        Properties prop = CommonsUtil.getTASProperties();
        
        String fileLoc = prop.getProperty("location.test");
        String fileName = "QR.png";
        QRUtil.encodeQR(url, fileLoc, fileName);
        
    }
}
