/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.util;

import java.util.Properties;
import junit.framework.TestCase;

/**
 *
 * @author Albert Vijai
 */
public class CommonsUtilTest extends TestCase {
    
    public CommonsUtilTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getTASProperties method, of class CommonsUtil.
     */
    public void testGetTASProperties() {
        System.out.println("getTASProperties");
        Properties prop = CommonsUtil.getTASProperties();
        
        System.out.println(prop.getProperty("location.base"));
        System.out.println(prop.getProperty("location.pdf"));
        System.out.println(prop.getProperty("location.qrc"));
        System.out.println(prop.getProperty("location.map"));
        System.out.println(prop.getProperty("location.test"));
        
    }
}
