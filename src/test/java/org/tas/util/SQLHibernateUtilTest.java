/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.util;

import java.io.File;
import java.util.List;
import junit.framework.TestCase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.tas.mysql.orm.Address;

/**
 *
 * @author Albert Vijai
 */
public class SQLHibernateUtilTest extends TestCase {
    
    public SQLHibernateUtilTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getSessionFactory method, of class MySQLHibernateUtil.
     */
    public void testGetSessionFactory() {
        System.out.println("getSessionFactory");
        
        SessionFactory factory = SQLHibernateUtil.getSessionFactory();
                //SQLHibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        List<Address> addressList = session.createSQLQuery("select * from tas.address").addEntity(Address.class).list();
        
//        for(Address aAddress: addressList){
//            System.err.println(aAddress.getLat()+", "+aAddress.getLong_());
//        }
        
        
        if(session.isOpen()){
            session.close();
        }
        }
    }
