delimiter $$

CREATE TABLE `address_load` (
  `id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `door_number` varchar(45) DEFAULT NULL,
  `apt_number` varchar(45) DEFAULT NULL,
  `street` varchar(250) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `latitude` decimal(12,10) DEFAULT NULL,
  `longitude` decimal(12,10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

delimiter $$

CREATE TABLE `address_stage` (
  `id` int(11) NOT NULL,
  `route_id` int(11) DEFAULT NULL,
  `territory_id` int(11) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(45) NOT NULL,
  `state` varchar(45) NOT NULL,
  `zip` int(11) NOT NULL,
  `latitude` decimal(12,10) DEFAULT NULL,
  `longitude` decimal(12,10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

delimiter $$


CREATE TABLE `territory` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `bounds` varchar(500) DEFAULT NULL,
  `address_count` int(11) DEFAULT NULL,
  `pdf_loc` varchar(500) DEFAULT NULL,
  `map_loc` varchar(500) DEFAULT NULL,
  `map_url` varchar(5000) DEFAULT NULL,
  `short_url` varchar(500) DEFAULT NULL,
  `territorycol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

delimiter $$

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `territory_id` int(11) DEFAULT NULL,
  `route_id` int(11) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `address` varchar(500) NOT NULL,
  `zip` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `state` varchar(45) NOT NULL,
  `latitude` decimal(12,10) NOT NULL,
  `longitude` decimal(12,10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_idx` (`territory_id`),
  CONSTRAINT `id` FOREIGN KEY (`territory_id`) REFERENCES `territory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$
