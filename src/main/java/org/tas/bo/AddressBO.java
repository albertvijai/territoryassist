/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.bo;

import java.math.BigDecimal;
import org.tas.mysql.orm.Address;

/**
 *
 * @author Albert Vijai
 */
public class AddressBO extends Address{
    
    
    public AddressBO(Address address){
       super.setId(address.getId());
       super.setName(address.getName());
       super.setPhone(address.getPhone());
       super.setAddress(address.getAddress());
       super.setZip(address.getZip());
       super.setCity(address.getCity());
       super.setState(address.getState());
       super.setLat(address.getLat());
       super.setLong_(address.getLong_());
       super.setFromCenter(address.getFromCenter());
       super.setSequenceNumber(address.getSequenceNumber());
       super.setTerritoryId(address.getTerritoryId());
    }
    
    private BigDecimal fromMinSeqNum;

    public BigDecimal getFromMinSeqNum() {
        return fromMinSeqNum;
    }

    public void setFromMinSeqNum(BigDecimal fromMinSeqNum) {
        this.fromMinSeqNum = fromMinSeqNum;
    }
    
    
    
}
