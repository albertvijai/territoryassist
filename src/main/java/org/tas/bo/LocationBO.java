/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.bo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Albert Vijai
 */
public class LocationBO {
    
    private String main_short;
    private List<Integer> locationSequence = new ArrayList<Integer>();

    public List<Integer> getLocationSequence() {
        return locationSequence;
    }

    public void setLocationSequence(List<Integer> locationSequence) {
        this.locationSequence = locationSequence;
    }

    public String getMain_short() {
        return main_short;
    }

    public void setMain_short(String main_short) {
        this.main_short = main_short;
    }
   
   
    
}
