/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.util;

import java.io.File;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.PropertyValueException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.criterion.Example;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Albert Vijai
 */
public class SQLHibernateUtil {

    private static final SessionFactory sessionFactory;
    
    static {
        try {
           File f = new File("C:\\workspace\\codebaseNetbeans\\territoryAssist\\src\\main\\resources\\org\\tas\\mysql\\orm\\hibernate.cfg.xml");
            sessionFactory = new AnnotationConfiguration().configure(f).buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    
       public static Session openSession() {
        Session ses = null;
        try {
            ses = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            if(e.getMessage().indexOf("No CurrentSessionContext configured") != -1) {
                ses = sessionFactory.openSession();
            }
        }
        return ses;
    }
    
    public static boolean saveHibernateObject(Object hibernateObject) throws Exception {
        boolean retVal = false;
        Session session = SQLHibernateUtil.openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate(hibernateObject);
            session.getTransaction().commit();
            retVal = true;
        } catch (HibernateException he) {
            session.getTransaction().rollback();
            throw new Exception(SQLHibernateUtil.getErrors(he));
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new Exception(e.getMessage());
        } finally {
            if(session.isOpen()) session.close();
        }
        return retVal;
    }

    public static boolean deleteHibernateObject(Object hibernateObject) throws Exception {
        boolean retVal = false;
        Session session = SQLHibernateUtil.openSession();
        try {
            session.beginTransaction();
            session.delete(hibernateObject);
            session.getTransaction().commit();
            retVal = true;
        } catch (HibernateException he) {
            session.getTransaction().rollback();
            throw new Exception(SQLHibernateUtil.getErrors(he));
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new Exception(e.getMessage());
        } finally {
            if(session.isOpen()) session.close();
        }
        return retVal;
    }

    public static Object getDbObj(Class ormClass, int id) throws Exception {
        Object dbObj = null;
        Session session = SQLHibernateUtil.openSession();
        try {
            session.beginTransaction();
            dbObj = session.get(ormClass, id);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            session.getTransaction().rollback();
            throw new Exception(SQLHibernateUtil.getErrors(he));
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw new Exception(e.getCause());
        } finally {
            if(session.isOpen()) session.close();
        }
        return dbObj;
    }

    public static Object getDbObj(Class ormClass, String id) throws Exception {
        Object dbObj = null;
        Session session = SQLHibernateUtil.openSession();
        try {
            session.beginTransaction();
            dbObj = session.get(ormClass, id);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            session.getTransaction().rollback();
            throw new Exception(SQLHibernateUtil.getErrors(he));
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw new Exception(e.getCause());
        } finally {
            if(session.isOpen()) session.close();
        }
        return dbObj;
    }    
    
    public static String getErrors(HibernateException he) {
        StringBuffer sb = new StringBuffer();
        for(String s : he.getMessages()) {
            if(sb.length() > 0) sb.append('\n');
            sb.append(s);
        }
        if(he instanceof PropertyValueException) {
            PropertyValueException pve = (PropertyValueException) he;
            if(pve != null) {
                if(sb.length() > 0) sb.append('\n');
                sb.append("EntityName: ")
                  .append(pve.getEntityName())
                  .append(" and PropertyName: ")
                  .append(pve.getPropertyName());
            }
        }
        return sb.toString();
    }

    public static String getErrors(SQLException se) {
        StringBuffer sb = new StringBuffer();
            System.out.println("Caught SQLException");
             while (se != null) {
                 if(sb.length() > 0) sb.append('\n');
                 sb.append(se.getMessage()).append(" errcode:").append(se.getErrorCode());
                 sb.append(" sqlstate:").append(se.getSQLState());
                 se = se.getNextException();
             }
            
        return sb.toString();
    }

    public static String getWarnings(SQLWarning w) {
        StringBuffer sb = new StringBuffer();
        while (w != null) {
            if(sb.length() > 0) sb.append('\n');
            sb.append(w.getMessage()).append(" sqlstate:").append(w.getSQLState());
            w = w.getNextWarning();
        }
        return sb.toString();
    }

    public static List getDbObjList(String hql) throws Exception {
        List rlist = null;
        Session session = SQLHibernateUtil.openSession();
        try {
            session.beginTransaction();
            rlist = session.createQuery(hql).list();
            session.getTransaction().commit();
        } catch (HibernateException he) {
            session.getTransaction().rollback();
            throw new Exception(SQLHibernateUtil.getErrors(he));
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw new Exception(e.getCause());
        } finally {
            if(session.isOpen()) session.close();
        }
        return rlist;
    }

    public static List getDbObjList(Class ormClass, Object ormObj) throws Exception {
        List rlist = null;
        Session session = SQLHibernateUtil.openSession();
        try {
            session.beginTransaction();
            Criteria crit = session.createCriteria(ormClass);
            crit.add(Example.create(ormObj).excludeZeroes());
            rlist = crit.list();
            session.getTransaction().commit();
        } catch (HibernateException he) {
            session.getTransaction().rollback();
            throw new Exception(SQLHibernateUtil.getErrors(he));
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw new Exception(e.getCause());
        } finally {
            if(session.isOpen()) session.close();
        }
        return rlist;
    }

    public static boolean saveHibernateObjectList(List objList) throws Exception {
        boolean retVal = false;
        Session session = SQLHibernateUtil.openSession();
        try {
            session.beginTransaction();
            Iterator it1 = objList.iterator();
            while(it1.hasNext()) {
                Object hibernateObject = it1.next();
                session.saveOrUpdate(hibernateObject);
            }
            session.getTransaction().commit();
            retVal = true;
        } catch (HibernateException he) {
            session.getTransaction().rollback();
            throw new Exception(SQLHibernateUtil.getErrors(he));
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new Exception(e.getMessage());
        } finally {
            if(session.isOpen()) session.close();
        }
        return retVal;
    }

    public static int updateDbObj(String hql) throws Exception {
        int count = 0;
        Session session = SQLHibernateUtil.openSession();
        try {
            session.beginTransaction();
            Query qry = session.createQuery(hql);
            count = qry.executeUpdate();
            session.getTransaction().commit();
        }
        catch (HibernateException he) {
            session.getTransaction().rollback();
            throw new Exception(SQLHibernateUtil.getErrors(he));
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new Exception(e.getMessage());
        } finally {
            if(session.isOpen()) session.close();
        }
        return count;
    }
    
    public static int updateDbObjWithSqlQry(String sql) throws Exception {
        int count = 0;
        Session session = SQLHibernateUtil.openSession();
        try {
            session.beginTransaction();
            count = session.createSQLQuery(sql).executeUpdate();
            session.getTransaction().commit();
        } catch (HibernateException he) {
            session.getTransaction().rollback();
            count = -1;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            count = -1;
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }
        return count;
    }
    
    
    
}
