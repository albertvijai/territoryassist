/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.util;

import java.io.IOException;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

/**
 *
 * @author Albert Vijai
 */
public class HTTPClientUtil {
    
    public byte[] getHTTPResource(String url){
        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);
        byte[] responseBody = null;
        //Default retry handler
        client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
                new DefaultHttpMethodRetryHandler());
        try {
                // Execute the method.
                int statusCode = client.executeMethod(method);

                if (statusCode == HttpStatus.SC_OK) 
                     responseBody = method.getResponseBody();
        } catch (HttpException e) {
          System.err.println("Fatal protocol violation: " + e.getMessage());
          e.printStackTrace();
        } catch (IOException e) {
          System.err.println("Fatal transport error: " + e.getMessage());
          e.printStackTrace();
        } finally {
          // Release the connection.
          method.releaseConnection();
        }  
        
      return   responseBody;
    }
    
    
     public String getHTTPResourceAsString(String url){
        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);
        String responseBody = null;
        //Default retry handler
        client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
                new DefaultHttpMethodRetryHandler());
        try {
            // Execute the method.
            int statusCode = client.executeMethod(method);
            if (statusCode == HttpStatus.SC_OK) 
                 responseBody = method.getResponseBodyAsString();
        } catch (HttpException e) {
          System.err.println("Fatal protocol violation: " + e.getMessage());
          e.printStackTrace();
        } catch (IOException e) {
          System.err.println("Fatal transport error: " + e.getMessage());
          e.printStackTrace();
        } finally {
          // Release the connection.
          method.releaseConnection();
        }  
        
      return   responseBody;
    }
}
