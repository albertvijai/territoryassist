/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tester;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Comparator;

/**
 * Does a multi-level sort on a list of <T> objects. Sorting can be 3 levels deep
 * and so, this class allows 3 sets of sorting data to be supplied.
 * paramName1 - First level sort attribute
 * paramType1 - Sort Type (asc/desc) for paramName1
 * paramName2 - Second level sort attribute
 * paramType2 - Sort Type (asc/desc) for paramName2
 * paramName3 - Third level sort attribute
 * paramType3 - Sort Type (asc/desc) for paramName3
 *
 * @author consult
 */
public class MultiLevelSorter<T> implements Comparator<T> {

    private String paramName1;
    private String paramType1;

    private String paramName2;
    private String paramType2;

    private String paramName3;
    private String paramType3;
    
    //Parameters to hold the paging information
    private int startRecNum;
    private int recBlockSize = 20;

    public String getParamName1() {
        return paramName1;
    }

    public void setParamName1(String paramName1) {
        this.paramName1 = paramName1;
    }

    public String getParamName2() {
        return paramName2;
    }

    public void setParamName2(String paramName2) {
        this.paramName2 = paramName2;
    }

    public String getParamName3() {
        return paramName3;
    }

    public void setParamName3(String paramName3) {
        this.paramName3 = paramName3;
    }

    public String getParamType1() {
        return paramType1;
    }

    public void setParamType1(String paramType1) {
        if(paramType1 != null)
            this.paramType1 = paramType1.toLowerCase();
        else
            this.paramType1 = null;        
    }

    public String getParamType2() {
        return paramType2;
    }

    public void setParamType2(String paramType2) {
        if(paramType2 != null)
            this.paramType2 = paramType2.toLowerCase();
        else
            this.paramType2 = null;
    }

    public String getParamType3() {
        return paramType3;
    }

    public void setParamType3(String paramType3) {
        if(paramType3 != null)
            this.paramType3 = paramType3.toLowerCase();
        else
            this.paramType3 = null;
    }

    public int getRecBlockSize() {
        return recBlockSize;
    }

    public void setRecBlockSize(int recBlockSize) {
        this.recBlockSize = recBlockSize;
    }

    public int getStartRecNum() {
        return startRecNum;
    }

    public void setStartRecNum(int startRecNum) {
        this.startRecNum = startRecNum;
    }

    public int compare(T o1, T o2) {
        int result = -2;
        try {
            if(paramName1 != null && paramName1.length() > 0) {
               String paramName1ObjType = getObjectType(o1, paramName1);
               result = compareParams(o1, o2, paramName1, paramType1, paramName1ObjType);
               if(result == 0) {
                   if(paramName2 != null && paramName2.length() > 0) {
                       String paramName2ObjType = getObjectType(o1, paramName2);
                       result = compareParams(o1, o2, paramName2, paramType2, paramName2ObjType);
                       if(result == 0) {
                           if(paramName3 != null && paramName3.length() > 0) {
                               String paramName3ObjType = getObjectType(o1, paramName3);
                               result = compareParams(o1, o2, paramName3, paramType3, paramName3ObjType);
                           }
                       }
                   }
               }
               return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();//TODO need to do exception handling later
        }
        return 0;
    }

    private int compareParams(T o1, T o2, String paramName, String paramType, String paramObjType) throws Exception {
        int result = -2;
        if("int".equals(paramObjType)
            || "java.lang.Integer".equals(paramObjType)) {
            Integer o1Int = (Integer) getObjectMember(o1, paramName);
            Integer o2Int = (Integer) getObjectMember(o2, paramName);
            if(o1Int == null)
                o1Int = new Integer(0);
            if(o2Int == null)
                o2Int = new Integer(0);
            if("desc".equals(paramType))
                result = o2Int.compareTo(o1Int);
            else
                result = o1Int.compareTo(o2Int);
        }
        else if("float".equals(paramObjType)
            || "java.lang.Float".equals(paramObjType)) {
            Float o1Float = (Float) getObjectMember(o1, paramName);
            Float o2Float = (Float) getObjectMember(o2, paramName);
            if(o1Float == null)
                o1Float = new Float(0.00);
            if(o2Float == null)
                o2Float = new Float(0.00);
            if("desc".equals(paramType))
                result = o2Float.compareTo(o1Float);
            else
                result = o1Float.compareTo(o2Float);
        }
        else if("double".equals(paramObjType)
            || "java.lang.Double".equals(paramObjType))  {
            Double o1Dbl = (Double) getObjectMember(o1, paramName);
            Double o2Dbl = (Double) getObjectMember(o2, paramName);
            if(o1Dbl == null)
                o1Dbl = new Double(0.00);
            if(o2Dbl == null)
                o2Dbl = new Double(0.00);
            if("desc".equals(paramType))
                result = o2Dbl.compareTo(o1Dbl);
            else
                result = o1Dbl.compareTo(o2Dbl);
        }
        else if("char".equals(paramObjType)
             || "java.lang.Character".equals(paramObjType)) {
            Character o1Ch = (Character) getObjectMember(o1, paramName);
            Character o2Ch = (Character) getObjectMember(o2, paramName);
            if(o1Ch == null)
                o1Ch = new Character(Character.MIN_VALUE);
            if(o2Ch == null)
                o2Ch = new Character(Character.MIN_VALUE);
            if("desc".equals(paramType))
                result = o2Ch.compareTo(o1Ch);
            else
                result = o1Ch.compareTo(o2Ch);
        }
        else if("boolean".equals(paramObjType)
             || "java.lang.Boolean".equals(paramObjType)) {
            Boolean o1Bool = (Boolean) getObjectMember(o1, paramName);
            Boolean o2Bool = (Boolean) getObjectMember(o2, paramName);
            if(o1Bool == null)
                o1Bool = false;
            if(o2Bool == null)
                o2Bool = false;
            if("desc".equals(paramType))
                result = o2Bool.compareTo(o1Bool);
            else
                result = o1Bool.compareTo(o2Bool);
        }
        else if("short".equals(paramObjType)
             || "java.lang.Short".equals(paramObjType)) {
            Short o1Short = (Short) getObjectMember(o1, paramName);
            Short o2Short = (Short) getObjectMember(o2, paramName);
            //TODO Add handling for null fields
            if("desc".equals(paramType))
                result = o2Short.compareTo(o1Short);
            else
                result = o1Short.compareTo(o2Short);
        }
        else if("long".equals(paramObjType)
             || "java.lang.Long".equals(paramObjType)) {
            Long o1Long = (Long) getObjectMember(o1, paramName);
            Long o2Long = (Long) getObjectMember(o2, paramName);
            if(o1Long == null)
                o1Long = new Long(0);
            if(o2Long == null)
                o2Long = new Long(0);
            if("desc".equals(paramType))
                result = o2Long.compareTo(o1Long);
            else
                result = o1Long.compareTo(o2Long);
        }
        else if("byte".equals(paramObjType)
             || "java.lang.Byte".equals(paramObjType)) {
            Byte o1Byte = (Byte) getObjectMember(o1, paramName);
            Byte o2Byte = (Byte) getObjectMember(o2, paramName);
            //TODO Add handling for null fields
            if("desc".equals(paramType))
                result = o2Byte.compareTo(o1Byte);
            else
                result = o1Byte.compareTo(o2Byte);
        }
        else if ("java.math.BigDecimal".equals(paramObjType)) {
            BigDecimal o1BigDec = (BigDecimal) getObjectMember(o1, paramName);
            BigDecimal o2BigDec = (BigDecimal) getObjectMember(o2, paramName);
            if(o1BigDec == null)
                o1BigDec = new BigDecimal(0.00);
            if(o2BigDec == null)
                o2BigDec = new BigDecimal(0.00);
            if("desc".equals(paramType))
                result = o2BigDec.compareTo(o1BigDec);
            else
                result = o1BigDec.compareTo(o2BigDec);
        }
        else if ("java.math.BigInteger".equals(paramObjType)) {
            BigInteger o1BigDec = (BigInteger) getObjectMember(o1, paramName);
            BigInteger o2BigDec = (BigInteger) getObjectMember(o2, paramName);
            if(o1BigDec == null)
                o1BigDec = new BigInteger("0");
            if(o2BigDec == null)
                o2BigDec = new BigInteger("0");
            if("desc".equals(paramType))
                result = o2BigDec.compareTo(o1BigDec);
            else
                result = o1BigDec.compareTo(o2BigDec);
        }                
        else if ("java.lang.String".equals(paramObjType)) {
            String o1Str = (String) getObjectMember(o1, paramName);
            String o2Str = (String) getObjectMember(o2, paramName);
            if(o1Str == null)
                o1Str = "";
            if(o2Str == null)
                o2Str = "";
            if("desc".equals(paramType))
                result = o2Str.compareTo(o1Str);
            else
                result = o1Str.compareTo(o2Str);
        }
        else if ("java.util.Date".equals(paramObjType)) {
            java.util.Date o1Date = (java.util.Date) getObjectMember(o1, paramName);
            java.util.Date o2Date = (java.util.Date) getObjectMember(o2, paramName);
            if(o1Date == null)
                o1Date = new java.util.Date();
            if(o2Date == null)
                o2Date = new java.util.Date();
            if("desc".equals(paramType))
                result = o2Date.compareTo(o1Date);
            else
                result = o1Date.compareTo(o2Date);
        }
        return result;
    }

    public boolean hasValidSortParams(Class<T> someClass) {
        
        boolean retval = false;
        
        String paramNameObjType = null;
        
        try {
            
            Object o1 = someClass.newInstance();
            
            if (paramName1 != null && paramName1.length() > 0) {
                paramNameObjType = getObjectType(o1, paramName1);
            }
            if (paramName2 != null && paramName2.length() > 0) {
                paramNameObjType = getObjectType(o1, paramName2);
            }
            if (paramName3 != null && paramName3.length() > 0) {
                paramNameObjType = getObjectType(o1, paramName3);
            }
            retval = (paramNameObjType != null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return retval;
    }
    
    public static String getObjectType(Object obj, String fieldName) throws
	    IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
		Class c = obj.getClass();
		String methodName = "get" + fieldName.toUpperCase().substring(0, 1) + fieldName.substring(1);
		Method m = c.getMethod(methodName);
		return m.getReturnType().getName();
	}
	
	public static Object getObjectMember(Object obj, String fieldName) throws
	    IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
		Class c = obj.getClass();
		Object fieldValue = null;
		String methodName = "get" + fieldName.toUpperCase().substring(0, 1) + fieldName.substring(1);
		Method m = c.getMethod(methodName);
		fieldValue = m.invoke(obj, (Object[]) null);
		return fieldValue;
	}
        
}
