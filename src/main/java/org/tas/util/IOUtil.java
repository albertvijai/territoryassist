/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.util;

import java.io.FileOutputStream;

/**
 *
 * @author Albert Vijai
 */
public class IOUtil {
    
    public void writeFile(String fileLoc, String fileName, byte[] content){
        try{
           FileOutputStream fos = new FileOutputStream(fileLoc+fileName);
           fos.write(content);
           fos.close();
        }catch (Exception e){
            System.err.println("Error: " + e.getMessage());
        }
    }
    
    
    
}
