/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.util;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import java.net.URI;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import org.tas.bo.LocationBO;


/**
 *
 * @author Albert Vijai
 */
public class MapUtil {
    
    public static float distBetween2GeoLoc(float lat1, float lng1, float lat2, float lng2) {
	    
        double earthRadius = 3958.75;
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                   Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                   Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = earthRadius * c;
        int meterConversion = 1609;

        return new Float(dist).floatValue();
    }
    
    
    public LocationBO resequenceAddress(String geoString){
         LocationBO location = null;
         String urlstring = "http://www.mapquestapi.com/directions/v1/optimizedroute?json={locations:["+geoString+"]}";
         try{
              String key = "&key=Fmjtd%7Cluuanuutn1%2C85%3Do5-96blg0";
              Client client = Client.create();
              URL url = new URL(urlstring);
              String nullFragment = null;
              URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(), url.getQuery(), nullFragment);
              WebResource webResource = client.resource(uri.toString()+key);
              String op = webResource.get(String.class);
              String jsonLocationSeq = "{\""+op.substring(op.indexOf("locationSequence"), op.indexOf("sessionId")-2)+"}";
              Gson gson = new Gson();
              location = gson.fromJson(jsonLocationSeq, LocationBO.class);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return location; 
    }
    
    public LocationBO getMapURL(String geoString){
         LocationBO location = null;
         String urlstring = "http://www.mapquest.com/_svc/linkgenerator?"+geoString+"forceresolve=no&withmodel=no&withshort=yes&withlinks=no&icid=103262";
         try{
              
              Client client = Client.create();
              URL url = new URL(urlstring);
              String nullFragment = null;
              URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(), url.getQuery(), nullFragment);
              WebResource webResource = client.resource(uri.toString());
              String op = webResource.get(String.class);
              String jsonLocationSeq = "{\""+op.substring(op.indexOf("main_short"), op.indexOf("embed_short")-2)+"}";
              Gson gson = new Gson();
              location = gson.fromJson(jsonLocationSeq, LocationBO.class);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return location; 
    }

    
}
 