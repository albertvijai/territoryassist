/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Albert Vijai
 */
public class CommonsUtil {
    
    public static Properties getTASProperties(){
        Properties prop = new Properties();
        try {
                String fileIO = "C:\\workspace\\codebaseNetbeans\\territoryAssist\\src\\main\\resources\\org\\tas\\mysql\\orm\\tas.properties";
    		prop.load(new FileInputStream(fileIO));
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
        return prop;
    }
    
}
