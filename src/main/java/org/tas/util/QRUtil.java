/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.util;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.io.File;
import java.io.FileOutputStream;

/**
 *
 * @author Albert Vijai
 */
public class QRUtil {
    
    public static void encodeQR(String url, String fileLoc, String fileName) throws Exception {
       int width = 400;
       int height = 300; 
       String imageFormat = "png"; // could be "gif", "tiff", "jpeg" 
       BitMatrix bitMatrix = new QRCodeWriter().encode(url, BarcodeFormat.QR_CODE, width, height);
       MatrixToImageWriter.writeToStream(bitMatrix, imageFormat, new FileOutputStream(new File(fileLoc+fileName)));
    }
    
}
