/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.dojo;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import org.tas.bo.AddressBO;
import org.tas.bo.LocationBO;
import org.tas.impl.AddressImpl;
import org.tas.impl.TerritoryImpl;
import org.tas.mysql.orm.Address;
import org.tas.mysql.orm.Territory;
import org.tas.util.CommonsUtil;
import org.tas.util.MapUtil;
import org.tas.util.QRUtil;
import tester.MultiLevelSorter;

/**
 *
 * @author Albert Vijai
 */
public class TasLoader {
    
    MapUtil mapUtil = new MapUtil();
    AddressImpl addressImpl = new AddressImpl();
    TerritoryImpl territoryImpl = new TerritoryImpl();
    
    public static void main(String[] args){
        TasLoader tasLoader = new TasLoader();
        tasLoader.doTASLoad();
    }
    
   /* -------------------------------------------------------------------------------------------------------------------------
    * ASSUMPTIONS: 
    * ------------
    * - Center is the KingdomHall[KH]
    * - Each territory contains <>20 addresses.[Mapquest can only optimaze max 26 address]
    * - Routing accuracy is NOT 100%
    * - Territory radius is not constant
    * - Output is as good as the input supplied
    * 
    * LOGIC:
    * ----------
    * - Iterate each address calculate and set distance from center[KingdomHall] in miles
    * - Get the closest address from center where territory_id is NULL and set sequence number to 0     
    * - Find the relative distance for the next 19 addresses and increment sequence number by 1 respectively
    * - Make a REST-WS call to MapQuest to optimize address between sequence numbers 0 - 20, i.e 2 thru 19, update the same
    * - Create a territory and update the territory to the above addresses
    * - Create a MapQuestShortURL for the territory and update territory
    * - Create a QR code for the above URL and update QR location territory
    * - Save/Update territory & address
    * - Repeat till all the addresses are processed
    * - Iterate territory and create a HTML file
    * - OPTIONAL [Google Chrome interim solution]: Convert PDF from XHTML using FlyingSauser java utility
    * -------------------------------------------------------------------------------------------------------------------------
    */
    protected  void doTASLoad(){
        try{
            generateDistanceFromCenter(getCenterLocation());
            while(null!=addressImpl.getFirstUnprocessedAddress()){
                Address firstAddress = addressImpl.getFirstUnprocessedAddress();
                if(null!=firstAddress){
                    //List that holds optimized addresses
                    List<AddressBO> optimizedAddressList = new ArrayList<AddressBO>();
                    //Set the first address
                    AddressBO firstAddressBO = new AddressBO(firstAddress);
                    firstAddressBO.setSequenceNumber(0);
                    optimizedAddressList.add(firstAddressBO);
                    //First point of processing
                    addressImpl.updateZeroSequence(firstAddress.getId());
                    //Get unprocessed addresses
                    List<AddressBO> unprocessedAddresses = getAddressBO(addressImpl.getAllAddresses());
                    calculateDistanceFromZeroSeq(firstAddress, unprocessedAddresses);
                    //Get sequence of address based on a birds flight path approach
                    optimizedAddressList.addAll(getNext19Addresses(unprocessedAddresses));
                    LocationBO location = getReOptimizedSequence(optimizedAddressList);
                    //Resequence the addresses based on travelling sales man approach
                    resequenceAddresses(optimizedAddressList, location);
                    addressImpl.updateSeqNumber(optimizedAddressList);
                    //Create and link territory to addresses
                    addressImpl.updateTerritoryId(optimizedAddressList, territoryImpl.insertTerritory(createTerritory(optimizedAddressList, getCenterLocation())));
                }
            }////Continue till all unprocessed addresses are processed
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    private Territory createTerritory(List<AddressBO> optimizedAddressList, Address center){
        Territory territory = new Territory();
        territory.setDate(new Date());
        territory.setName(getTerritoryName(optimizedAddressList, center));
        territory.setMapUrl(getTerritoryMapURL(optimizedAddressList));
        territory.setQrLocation(getTerritoryQR(territory.getMapUrl()));
        return territory;
    }
    
    private String getTerritoryName(List<AddressBO> optimizedAddressList, Address center){
         Float dfc = MapUtil.distBetween2GeoLoc(center.getLat().floatValue(), center.getLong_().floatValue(), 
                                                       optimizedAddressList.get(0).getLat().floatValue(), optimizedAddressList.get(0).getLong_().floatValue());
        
        String name = optimizedAddressList.get(0).getState() + "-" 
                     +optimizedAddressList.get(0).getCity() + "-"
                     +optimizedAddressList.get(0).getZip() + "-"
                     +String.format("%.2f", dfc) + "M";
                      
        return name;
    }
    
    private String getTerritoryMapURL(List<AddressBO> optimizedAddressList){
        String mapURL = "";
        MultiLevelSorter<AddressBO> mls = new MultiLevelSorter<AddressBO>();
        mls.setParamName1("sequenceNumber");
        Collections.sort(optimizedAddressList, mls);
        StringBuffer buff = new StringBuffer("");
        for(int i=0;i<optimizedAddressList.size();i++){
            AddressBO addressBO = optimizedAddressList.get(i);
            buff.append("q").append(i+1).append("=").append(addressBO.getLat()).append(",").append(addressBO.getLong_()).append("(").append(i+1).append(")").append("&");
        }
        mapURL = mapUtil.getMapURL(buff.toString()).getMain_short();
        return mapURL;
    }
    
    private String getTerritoryQR(String mapURL){
        Properties prop = CommonsUtil.getTASProperties();
        String fileLoc = prop.getProperty("location.qrc");
        String newstring = new SimpleDateFormat("yyyyMMddss").format(new Date());
        String fileName = "QR"+newstring+".png";
        try{
            QRUtil.encodeQR(mapURL, fileLoc, fileName);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return fileName;
    }
    
    
    private void resequenceAddresses(List<AddressBO> optimizedAddressList, LocationBO location){
        List<Integer> newSequenceNumbers = location.getLocationSequence();
        if(null!=optimizedAddressList && !optimizedAddressList.isEmpty()){
            AddressBO addressBO = null;
            for(int i =0;i<optimizedAddressList.size();i++){
                 addressBO = optimizedAddressList.get(i);
                 addressBO.setSequenceNumber((newSequenceNumbers.get(i)));
            }
        }
    }
    
    
    private LocationBO getReOptimizedSequence(List<AddressBO> optimizedAddressList){
       LocationBO locationBO = null;
       StringBuffer buff = new StringBuffer(); 
       if(null!=optimizedAddressList && !optimizedAddressList.isEmpty()){
            AddressBO addressBO = null;
            for(AddressBO aAddressBO: optimizedAddressList){
                buff.append("'"+aAddressBO.getLat()+","+aAddressBO.getLong_()+"',");
            }
        }
        String geoString = buff.toString().substring(0, buff.toString().length()-1);
        locationBO = mapUtil.resequenceAddress(geoString);
        return locationBO;
    }
    
    private List<AddressBO> getAddressBO(List<Address> unprocessedAddressList){
        AddressBO addressBO = null;
        List<AddressBO> unprocessedAddressBOList = new ArrayList<AddressBO>();
        if(null!=unprocessedAddressList && !unprocessedAddressList.isEmpty()){
            for(Address aAddress: unprocessedAddressList){
                addressBO = new AddressBO(aAddress);
                unprocessedAddressBOList.add(addressBO);
            }
        }
        return unprocessedAddressBOList;
    }
    
    private List<AddressBO> getNext19Addresses(List<AddressBO> unprocessedAddressList){
        MultiLevelSorter<AddressBO> mls = new MultiLevelSorter<AddressBO>();
        mls.setParamName1("fromMinSeqNum");
        Collections.sort(unprocessedAddressList, mls);
        AddressBO addressBO = null;
        List<AddressBO> top19Addresses = new ArrayList<AddressBO>();
        if(unprocessedAddressList.size()>1){
            int listSize = (unprocessedAddressList.size()<=19)?unprocessedAddressList.size():19;
            for(int i=0; i<listSize;i++){
                addressBO = unprocessedAddressList.get(i);
                addressBO.setSequenceNumber(i+1);
                top19Addresses.add(addressBO);
            }
        }
        return top19Addresses;
    }
    
    private void calculateDistanceFromZeroSeq(Address seqZeroAddress, List<AddressBO> unprocessedAddressList){
        if(null!= seqZeroAddress && null!=unprocessedAddressList && !unprocessedAddressList.isEmpty()){
            for(AddressBO aAddress : unprocessedAddressList){
                Float dfc = MapUtil.distBetween2GeoLoc(seqZeroAddress.getLat().floatValue(), seqZeroAddress.getLong_().floatValue(), 
                                                       aAddress.getLat().floatValue(), aAddress.getLong_().floatValue());
                aAddress.setFromMinSeqNum(new BigDecimal(String.format("%.2f", dfc)));
            }
        }
    }
    
    
    private void generateDistanceFromCenter(Address kingdomHall) throws Exception{
        List<Address> addressList = addressImpl.getAllAddresses();
        if(null!=addressList && !addressList.isEmpty()){
            for(Address aAddress : addressList){
                Float dfc = MapUtil.distBetween2GeoLoc(kingdomHall.getLat().floatValue(), kingdomHall.getLong_().floatValue(), 
                                                       aAddress.getLat().floatValue(), aAddress.getLong_().floatValue());
                aAddress.setFromCenter(new BigDecimal(String.format("%.2f", dfc)));
            }
        }
        addressImpl.updateDistanceFromCenter(addressList);
    }
    
    
    //TODO: Move address to property file.
    private Address getCenterLocation(){
        Address center = new Address();
        center.setAddress("2804 Rock Island Rd Irving TX  75060-2205");
        //32.815440, -96.983440
        center.setLat(BigDecimal.valueOf(32.815440));
        center.setLong_(BigDecimal.valueOf(-96.983440));
        return center;
    }
    
}
