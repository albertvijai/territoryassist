/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.tas.mysql.orm.Territory;
import org.tas.util.SQLHibernateUtil;

/**
 *
 * @author Albert Vijai
 */
public class TerritoryImpl {
    
    public int insertTerritory(Territory t){
        int id = 0;
        Session session = SQLHibernateUtil.openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate(t);
            id = t.getId();
            session.getTransaction().commit();
        } catch (HibernateException he) {
            session.getTransaction().rollback();
            
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            if(session.isOpen()) session.close();
        }
        
        return id;
    }
    
    
}
