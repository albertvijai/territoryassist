/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tas.impl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.tas.bo.AddressBO;
import org.tas.mysql.orm.Address;
import org.tas.util.SQLHibernateUtil;

/**
 *
 * @author Albert Vijai
 */
public class AddressImpl {
    
    public List<Address> getAllAddresses(){
        List<Address> addressList = new ArrayList<Address>();
        try{
            Session session = SQLHibernateUtil.openSession();
            addressList = session.createSQLQuery("select * from tas.address where territory_id is null and sequence_number is null").addEntity(Address.class).list();
            
            if(session.isOpen())
                session.close();
        }
        catch(HibernateException hex){
            hex.printStackTrace();
        }
        return addressList;
    }
    
    public List<AddressBO> getAllAddressesBO(){
        List<AddressBO> addressList = new ArrayList<AddressBO>();
        try{
            Session session = SQLHibernateUtil.openSession();
            addressList = session.createSQLQuery("select * from tas.address where territory_id is null and sequence_number is null").setResultTransformer(Transformers.aliasToBean(AddressBO.class)).list();
            
            if(session.isOpen())
                session.close();
        }
        catch(HibernateException hex){
            hex.printStackTrace();
        }
        return addressList;
    }
    
    public boolean updateDistanceFromCenter(List<Address> addressList){
        try{
            if(null!=addressList){
                for(Address aAddress : addressList){
                    SQLHibernateUtil.updateDbObjWithSqlQry("UPDATE TAS.ADDRESS SET FROM_CENTER = "+aAddress.getFromCenter()+" WHERE ID="+aAddress.getId());
                }
            }
        }
        catch(HibernateException hex){
            hex.printStackTrace();
        }
        catch(Exception hex){
            hex.printStackTrace();
        }
    
        return true;
    }
    
   
   public boolean updateTerritoryId(List<AddressBO> addressList, int id){
        try{
            if(null!=addressList){
                for(AddressBO aAddress : addressList){
                    SQLHibernateUtil.updateDbObjWithSqlQry("UPDATE TAS.ADDRESS SET TERRITORY_ID = "+id+" WHERE ID="+aAddress.getId());
                }
            }
        }
        catch(HibernateException hex){
            hex.printStackTrace();
        }
        catch(Exception hex){
            hex.printStackTrace();
        }
    
        return true;
    } 
    
    
   public boolean updateSeqNumber(List<AddressBO> addressList){
        try{
            if(null!=addressList){
                for(AddressBO aAddress : addressList){
                    SQLHibernateUtil.updateDbObjWithSqlQry("UPDATE TAS.ADDRESS SET SEQUENCE_NUMBER = "+aAddress.getSequenceNumber()+" WHERE ID="+aAddress.getId());
                }
            }
        }
        catch(HibernateException hex){
            hex.printStackTrace();
        }
        catch(Exception hex){
            hex.printStackTrace();
        }
    
        return true;
    }
    
    
    public boolean updateZeroSequence(int id){
        try{
           
                SQLHibernateUtil.updateDbObjWithSqlQry("UPDATE TAS.ADDRESS SET SEQUENCE_NUMBER = 0 WHERE ID="+id);
                
        }
        catch(HibernateException hex){
            hex.printStackTrace();
        }
        catch(Exception hex){
            hex.printStackTrace();
        }
    
        return true;
    }
    
    
    
    public Address getFirstUnprocessedAddress() throws Exception{
        List<Address> rlist = null;
        Address address = null;
        Session session = SQLHibernateUtil.openSession();
        try {
            session.beginTransaction();
            rlist = session.createSQLQuery("select * from tas.address where territory_id is null order by from_center limit 1").addEntity(Address.class).list();
            if(null!=rlist && !rlist.isEmpty()){
                address = rlist.get(0);
            }
            session.getTransaction().commit();
        } catch (HibernateException he) {
            session.getTransaction().rollback();
            throw new Exception(SQLHibernateUtil.getErrors(he));
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw new Exception(e.getCause());
        } finally {
            if(session.isOpen()) session.close();
        }
        return address;
    }
    
}
